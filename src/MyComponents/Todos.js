import React from 'react'
import {TodoItem} from './TodoItem'

export const Todos = (props) => {
  let mystyle={
    minHeight:"70vh",
    margin:"40px auto",
  }
  return (
    <div className='container' style={mystyle}>
      <h3 className='my-3' >Todos List</h3>
      
      {props.todos.length===0? "Well done butter you deleted everything :)" : (props.todos.map((todo)=>{
        return (
        <div key={todo.sno}>
        <TodoItem todo={todo}  onDelete={props.onDelete}/><hr></hr>
        </div>
        )
      }))}
      {/* <TodoItem todo={props.todos[2]}/> */}
    </div>
  )
}
